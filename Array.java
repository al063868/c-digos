/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package array;

public class Array {

       public static void main(String[] args) {
        
        String[] nombres = {"Estefania", "Daniel", "Frida","Eduardo","Yazuri"};
        
        int[] edadDatos = {20,20,20,20,20};
        
        
        int[] edad = new int [5] ;
        
        edad[0]=20;
        edad[1]=20; 
        edad[2]=20; 
        edad[3]=20; 
        edad[4]=20;
        
        
         System.out.println("NOMBRES DE PILA");
        
         for (int i = 0; i < nombres.length; i++){
    
         System.out.println(nombres[i]);
        
    }
     
         System.out.println("---------------------------------------------------------");
          
         System.out.println("EDADES");
         
         for (int i = 0; i < edad.length; i++){
    
         System.out.println(edad[i]);
         
         
          }
    }

}
