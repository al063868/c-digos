
Array of multiples

function arrayOfMultiples (num, length) {
	var arr = [];
	
	for(let i = 1; i <= length; i++){
		var number = num * i;
		arr.push(number)
	}
	return arr
}

-------------------------------------------------------------------------------------------------------------------------------

Burglary Series(04);

function addName (obj, name, value) {
  obj[name] = value;
  return obj;
}

-----------------------------------------------------------------------------------------------------------------------------

String Match by Two Letters

function strMatchBy2char(a, b) {
		let res = 0;
		for (let i = 0; i < a.length; i++) {
			if (a.slice(i, i + 2) === b.slice(i, i + 2) && a.slice(i, i + 2).length === 2)
			res += 1;
		}
		return res;
}


------------------------------------------------------------------------------------------------------------------------
Basic Calculator 

function calculator(num1, operator, num2) {
	operations = {
		'+': (a,b) => a + b,
		'-': (a,b) => a - b,
		'*': (a,b) => a * b,
		'/': (a,b) => b ? a / b :"Can't divide by 0!"
	}
	
	return operations[operator](num1,num2)
}

------------------------------------------------------------------------------------------------------------------------
Remove the letters ABC

function removeABC(str) {
	if (str.includes("a")|| str.includes("b") || str.includes("c")){
		return str.match(/[^abc]/gi).join("")
	}
	else {
		return null;
	}
}